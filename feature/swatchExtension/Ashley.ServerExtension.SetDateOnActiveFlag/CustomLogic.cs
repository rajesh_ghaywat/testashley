﻿using inRiver.Remoting.Log;
using inRiver.Remoting.Objects;
using inRiver.Server.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ashley.ServerExtension.SetDateOnActiveFlag
{
    /// <summary>
    /// Handle the custom logic of setting the change date on Status flag is set
    /// </summary>
    public class CustomLogic
    {

        #region Public Methods

        /// <summary>
        /// Set change date on ActiveStatus flag is set when entity is Added
        /// </summary>
        /// <param name="entity"></param>
        public void SetActiveStatusOnAdd(Entity entity)
        {
            Field changeStatusDateField = entity.Fields.FirstOrDefault(f => f.FieldType.Settings.ContainsKey(SettingName.UpdateDateOnActiveStatus.ToString()));

            if (changeStatusDateField != null)
            {
                ExtensionConfiguration entityProperties = ExtensionConfiguration.GetFieldConfigurations(changeStatusDateField);
                try
                {
                    if (entityProperties.FieldTypeConfigurationValidated && IsEntityThirdPartyFlagSet(entity, entityProperties))
                    {
                        Field currentActiveStatusField = entity.GetField(entityProperties.ActiveStatusFieldTypeId);

                        if (currentActiveStatusField != null && currentActiveStatusField.Data is bool)
                        {
                            bool activeStatusValue = (bool)currentActiveStatusField.Data;
                            if (activeStatusValue)
                            {
                                SetCurrentDateInField(changeStatusDateField);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExtensionService.Instance.Log(LogLevel.Error, "OnUpdate:Set changed date", ex, "", nameof(SetActiveStatusOnAdd));
                }
            }
        }

        /// <summary>
        /// Set change date on ActiveStatus flag is set when entity is updated
        /// </summary>
        /// <param name="entity"></param>
        public void SetActiveStatusOnUpdate(Entity entity)
        {
            try
            {
                Field changeStatusDateField = entity.Fields.FirstOrDefault(f => f.FieldType.Settings.ContainsKey(SettingName.UpdateDateOnActiveStatus.ToString()));

                if (changeStatusDateField != null)
                {
                    ExtensionConfiguration entityProperties = ExtensionConfiguration.GetFieldConfigurations(changeStatusDateField);

                    if (entityProperties.FieldTypeConfigurationValidated && IsEntityThirdPartyFlagSet(entity, entityProperties))
                    {
                        Field currentActiveStatusField = entity.GetField(entityProperties.ActiveStatusFieldTypeId);

                        if (currentActiveStatusField != null && currentActiveStatusField.Data is bool)
                        {
                            bool activeStatusValue = (bool)currentActiveStatusField.Data;
                            if (activeStatusValue)
                            {
                                Field existingActiveStatusField = ExtensionService.Instance.GetField(entity.Id, entityProperties.ActiveStatusFieldTypeId);

                                if (existingActiveStatusField != null)
                                {
                                    if (existingActiveStatusField.Data != null)
                                    {
                                        bool existingActiveStatusValue = (bool)existingActiveStatusField.Data;
                                        if (!existingActiveStatusValue)
                                        {
                                            SetCurrentDateInField(changeStatusDateField);
                                        }
                                    }
                                    else
                                    {
                                        SetCurrentDateInField(changeStatusDateField);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, "OnUpdate:Set changed date", ex, "", nameof(SetActiveStatusOnUpdate));
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Check whether the third party flag is set
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="entityProperties"></param>
        /// <returns></returns>
        private bool IsEntityThirdPartyFlagSet(Entity entity, ExtensionConfiguration entityProperties)
        {
            Field currentActiveStatusField = entity.GetField(entityProperties.ThirdPartyFlagFieldTypeId);

            if (currentActiveStatusField != null && currentActiveStatusField.Data is bool)
            {
                return (bool)currentActiveStatusField.Data;
            }

            return false;
        }

        /// <summary>
        /// Set current date in the provided field
        /// </summary>
        /// <param name="changeStatusDateField"></param>
        private void SetCurrentDateInField(Field changeStatusDateField)
        {
            if (changeStatusDateField != null)
            {
                changeStatusDateField.Data = DateTime.Now;
            }
        }

        #endregion
    }
}
