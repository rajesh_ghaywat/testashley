﻿using inRiver.Remoting.Log;
using inRiver.Remoting.Objects;
using inRiver.Server.Extension;
using System;
using System.Collections.Generic;
//using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ashley.ServerExtension.SetDateOnActiveFlag
{
    #region Public Enum

    /// <summary>
    /// The enum defines the setting names
    /// </summary>
    public enum SettingName
    {
        UpdateDateOnActiveStatus
    }

    #endregion

    /// <summary>
    /// It handles the configuration for the server extension
    /// </summary>
    public class ExtensionConfiguration
    {
        #region Private Constants

        const string BOOLEAN_FIELDTYPEID = "BooleanFieldTypeId";
        const string BOOLEAN_THIRDPARTYFLAG_FIELDTYPEID = "BooleanThirdPartyFlagFieldTypeId";
        const char DELIMETER = ';';

        #endregion

        #region Public Properties
        public string ActiveStatusFieldTypeId { get; set; }
        public string ThirdPartyFlagFieldTypeId { get; set; }
        public bool FieldTypeConfigurationValidated { get; set; }

        #endregion

        #region Private Properties
        private static ExtensionConfiguration ExtensionConfigurationValue { get; set; }
        #endregion

        #region Private Constructor

        private ExtensionConfiguration()
        {
        }
        #endregion

        #region Public Static Method

        /// <summary>
        /// It validates and initialize the configuration object.
        /// </summary>
        /// <param name="changeStatusDateField"></param>
        /// <returns></returns>
        public static ExtensionConfiguration GetFieldConfigurations(Field changeStatusDateField)
        {

            if (ExtensionConfigurationValue == null)
            {
                ExtensionConfigurationValue = new ExtensionConfiguration();
                string updateDateOnActiveStatusSetting = changeStatusDateField.FieldType.Settings[SettingName.UpdateDateOnActiveStatus.ToString()];

                if (!string.IsNullOrEmpty(updateDateOnActiveStatusSetting))
                {
                    string[] updateDateOnActiveStatusSettingkeyValues = updateDateOnActiveStatusSetting.Split(DELIMETER);
                    foreach (string updateDateOnActiveStatusSettingkeyValue in updateDateOnActiveStatusSettingkeyValues)
                    {
                        if (updateDateOnActiveStatusSettingkeyValue.Contains(BOOLEAN_FIELDTYPEID))
                        {
                            string booleanFieldTypeId = updateDateOnActiveStatusSettingkeyValue.Substring($"{BOOLEAN_FIELDTYPEID}=".Length);
                            if (string.IsNullOrEmpty(booleanFieldTypeId))
                            {
                                ExtensionService.Instance.Log(LogLevel.Error, $"The {BOOLEAN_FIELDTYPEID} setting is not provided properly in StausDate field setting", null, "", nameof(GetFieldConfigurations));
                                return ExtensionConfigurationValue;
                            }
                            FieldType booleanFieldType = ExtensionService.Instance.GetFieldType(booleanFieldTypeId);
                            if (booleanFieldType == null)
                            {
                                ExtensionService.Instance.Log(LogLevel.Error, $"The {BOOLEAN_FIELDTYPEID} setting  {booleanFieldType} is not provided properly in StausDate field setting", null, "", nameof(GetFieldConfigurations));
                                return ExtensionConfigurationValue;
                            }
                            ExtensionConfigurationValue.ActiveStatusFieldTypeId = booleanFieldType.Id;
                        }
                        else if (updateDateOnActiveStatusSettingkeyValue.Contains(BOOLEAN_THIRDPARTYFLAG_FIELDTYPEID))
                        {
                            string booleanThirdPartyFlagFieldTypeId = updateDateOnActiveStatusSettingkeyValue.Substring($"{BOOLEAN_THIRDPARTYFLAG_FIELDTYPEID}=".Length);
                            if (string.IsNullOrEmpty(booleanThirdPartyFlagFieldTypeId))
                            {
                                ExtensionService.Instance.Log(LogLevel.Error, $"The {BOOLEAN_THIRDPARTYFLAG_FIELDTYPEID} setting is not provided properly in StausDate field setting", null, "", nameof(GetFieldConfigurations));
                                return ExtensionConfigurationValue;
                            }
                            FieldType booleanThirdPartyFlagFieldType = ExtensionService.Instance.GetFieldType(booleanThirdPartyFlagFieldTypeId);
                            if (booleanThirdPartyFlagFieldType == null)
                            {
                                ExtensionService.Instance.Log(LogLevel.Error, $"The {BOOLEAN_THIRDPARTYFLAG_FIELDTYPEID} setting  {booleanThirdPartyFlagFieldType} is not provided properly in StausDate field setting", null, "", nameof(GetFieldConfigurations));
                                return ExtensionConfigurationValue;
                            }
                            ExtensionConfigurationValue.ThirdPartyFlagFieldTypeId = booleanThirdPartyFlagFieldType.Id;
                        }
                    }

                    ExtensionConfigurationValue.FieldTypeConfigurationValidated = true;
                }
            }
            return ExtensionConfigurationValue;
        }
        #endregion
    }
}
