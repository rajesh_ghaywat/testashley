﻿using inRiver.Remoting.Log;
using inRiver.Server.Extension;
using System;
using inRiver.Remoting.Objects;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ashley.ServerExtension.SetDateOnActiveFlag;

namespace Ashley.ServerExtension.SetDateOnActiveFlag
{
    /// <summary>
    /// This extension is implementation of US 11.1 -Set Changed Date
    /// When status is set to Active in inRiver we also need to update the Status change date automatically in inRiver with the date and time that the Status was updated to Active
    /// Write an extension to set the date and time based on status change.
    /// This logic is for 3rdParty SKUS that are not entered in PK.
    /// </summary>
    public class ServerExtension : IServerExtension
    {
        #region IServerExtension Methods
        private CustomLogic CustomLogic = new CustomLogic();

        /// <summary>
        /// This method will be called when entity is created
        /// </summary>
        /// <param name="entity">entity which is to be created</param>
        /// <param name="arg"></param>
        public void OnAdd(inRiver.Remoting.Objects.Entity entity, CancelUpdateArgument arg)
        {
            CustomLogic.SetActiveStatusOnAdd(entity);
        }

        public void OnCreateVersion(inRiver.Remoting.Objects.Entity entity, CancelUpdateArgument arg)
        {

        }

        public void OnDelete(inRiver.Remoting.Objects.Entity entity, CancelUpdateArgument arg)
        {

        }

        public void OnLink(inRiver.Remoting.Objects.Link link, CancelUpdateArgument arg)
        {

        }

        public void OnLinkUpdate(inRiver.Remoting.Objects.Link link, CancelUpdateArgument arg)
        {

        }

        public void OnLock(inRiver.Remoting.Objects.Entity entity, CancelUpdateArgument arg)
        {

        }

        public void OnUnlink(inRiver.Remoting.Objects.Link link, CancelUpdateArgument arg)
        {

        }

        public void OnUnlock(inRiver.Remoting.Objects.Entity entity, CancelUpdateArgument arg)
        {

        }

        /// <summary>
        /// This method will be called when entity is updated
        /// </summary>
        /// <param name="entity">entity which is to be created</param>
        /// <param name="arg"></param>
        public void OnUpdate(inRiver.Remoting.Objects.Entity entity, CancelUpdateArgument arg)
        {
            CustomLogic.SetActiveStatusOnUpdate(entity);
        }
        #endregion

    }
}
