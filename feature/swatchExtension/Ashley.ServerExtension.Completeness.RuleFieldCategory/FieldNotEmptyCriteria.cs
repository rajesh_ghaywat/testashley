﻿using inRiver.Server.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using inRiver.Remoting.Objects;
using inRiver.Remoting.Log;

namespace Ashley.ServerExtension.Completeness.RuleFieldCategory
{
    public class FieldNotEmptyCriteria : ICompletenessCriteria
    {
        #region Constants
        private const string CategoryIdKey = "CategoryId";
        private const string FieldSetIdKey = "FieldSetId";
        #endregion

        #region Overriden Methods
        public string Name
        {
            get
            {
                return "Field Not Empty For Category/FieldSet";
            }
        }

        public List<string> SettingsKeys
        {
            get
            {
                return new List<string>()
                {
                    CategoryIdKey,
                    FieldSetIdKey
                };
            }
        }

        public int GetCriteriaCompletenessPercentage(int entityId, List<CompletenessRuleSetting> settings)
        {
            int completenessPercentage = 0;
            CompletenessRuleSetting completenessRuleCategoryIdSetting = settings.FirstOrDefault<CompletenessRuleSetting>((Func<CompletenessRuleSetting, bool>)(s => s.Key == CategoryIdKey));
            CompletenessRuleSetting completenessRuleFieldSetIdSetting = settings.FirstOrDefault<CompletenessRuleSetting>((Func<CompletenessRuleSetting, bool>)(s => s.Key == FieldSetIdKey));
            try
            {
                if (completenessRuleCategoryIdSetting != null && !string.IsNullOrEmpty(completenessRuleCategoryIdSetting.Value))
                {
                    completenessPercentage = CalculateCompletenessForCategory(entityId, completenessRuleCategoryIdSetting.Value);
                }
                else
                {
                    if (!string.IsNullOrEmpty(completenessRuleFieldSetIdSetting.Value))
                    {
                        completenessPercentage = CalculateCompletenessForFieldSets(entityId, completenessRuleFieldSetIdSetting.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, $"Error occurred in GetCriteriaCompletenessPercentage for entity Id {entityId}", ex, string.Empty, "GetCriteriaCompletenessPercentage");
            }
            return completenessPercentage;
        }
        #endregion

        #region Public Section
        public int GetCompletenessFieldData(List<Field> fields)
        {
            int numberFieldIncomplete = 0;
            Decimal percentComplete = 0;

            if (!(fields != null && fields.Count > 0))
                return 0;

            foreach (Field field in fields)
            {
                if (field == null || field.Data == null || (string.IsNullOrEmpty(field.Data.ToString()) || field.Data.GetType() == typeof(LocaleString) && LocaleString.IsNullOrEmpty((LocaleString)field.Data)))
                {
                    numberFieldIncomplete++;
                }
            }
            percentComplete = ((((Decimal)(fields.Count) - (Decimal)numberFieldIncomplete) / (Decimal)fields.Count) * new Decimal(100));

            return (int)percentComplete;
        }
        #endregion

        #region Private Section
        private int CalculateCompletenessForCategory(int entityId, string categoryId)
        {
            try
            {
                if (string.IsNullOrEmpty(categoryId))
                    return 0;

                Entity entity = ExtensionService.Instance.GetEntity(entityId, LoadLevel.DataOnly);

                List<Field> fields = entity.Fields.Where(a => a.FieldType.CategoryId.Equals(categoryId)).ToList();

                return GetCompletenessFieldData(fields);
            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, $"Error occurred in CalculateCompletenessForCategory for entity Id {entityId}", ex, string.Empty, "CalculateCompletenessForCategory");
            }
            return 0;
        }


        private int CalculateCompletenessForFieldSets(int entityId, string fieldSetId)
        {
            try
            {
                Entity entity = ExtensionService.Instance.GetEntity(entityId, LoadLevel.Shallow);
                List<FieldSet> fieldSets = ExtensionService.Instance.GetFieldSetsForEntityType(entity.EntityType.Id);
                if (fieldSets == null)
                    return 0;

                FieldSet fieldSet = fieldSets.Where(w => w.Id.Equals(fieldSetId)).FirstOrDefault();
                if (fieldSet == null)
                    return 0;

                List<String> fieldSetFieldTypes = fieldSet.FieldTypes.ToList();
                List<Field> fields = ExtensionService.Instance.GetFields(entityId, fieldSetFieldTypes);

                return GetCompletenessFieldData(fields);
            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, $"Error occurred in CalculateCompletenessForFieldSets for entity Id {entityId}", ex, string.Empty, "CalculateCompletenessForFieldSets");
                return 0;
            }
        }
        #endregion
    }
}
