﻿using inRiver.Server.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using inRiver.Remoting.Objects;
using inRiver.Remoting.Log;
using inRiver.Remoting.Dto;
using System.Globalization;

namespace Ashley.ServerExtension.Completeness.RuleFieldLanguage
{
    public class FieldLanguageCriteria : ICompletenessCriteria
    {
        #region Constants
        private const string FieldTypeIdKey = "FieldTypeId";
        private const string LanguageKey = "LanguageCodes";
        #endregion

        #region Overriden Methods
        public string Name
        {
            get
            {
                return "Field Not Empty For Language";
            }
        }

        public List<string> SettingsKeys
        {
            get
            {
                return new List<string>()
                {
                    FieldTypeIdKey,
                    LanguageKey
                };
            }
        }

        public int GetCriteriaCompletenessPercentage(int entityId, List<CompletenessRuleSetting> settings)
        {
            int completenessPercentage = 0;
            CompletenessRuleSetting completenessRuleFieldTypeIdSetting = settings.FirstOrDefault<CompletenessRuleSetting>((Func<CompletenessRuleSetting, bool>)(s => s.Key == FieldTypeIdKey));
            CompletenessRuleSetting completenessRuleLanguageSetting = settings.FirstOrDefault<CompletenessRuleSetting>((Func<CompletenessRuleSetting, bool>)(s => s.Key == LanguageKey));
            List<CultureInfo> languageCuluterInfo = new List<CultureInfo>();
            try
            {
                if (completenessRuleFieldTypeIdSetting != null && !string.IsNullOrEmpty(completenessRuleFieldTypeIdSetting.Value) && completenessRuleLanguageSetting != null && !string.IsNullOrEmpty(completenessRuleLanguageSetting.Value))
                {
                    Field field = ExtensionService.Instance.GetField(entityId, completenessRuleFieldTypeIdSetting.Value);
                    if (field == null)
                        return 0;
                    ValidateLanguages(completenessRuleLanguageSetting.Value,  languageCuluterInfo);
                    completenessPercentage = CalculateCompletenessForLanguage(field, languageCuluterInfo);
                }
            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, $"Error occurred in GetCriteriaCompletenessPercentage for entity Id {entityId}", ex, string.Empty, "GetCriteriaCompletenessPercentage");
            }
            return completenessPercentage;
        }
        #endregion

        #region Private Method
        private int CalculateCompletenessForLanguage(Field field, List<CultureInfo> languages)
        {
            List<string> languageData = new List<string>();
            try
            {
                if (field.FieldType.DataType != DataType.LocaleString)
                    return 0;

                if (languages.Count == 0)
                    return 0;

                LocaleString fieldData = (LocaleString)field.Data;
                foreach (CultureInfo lang in languages)
                {
                    string data = fieldData[lang];
                    if (!string.IsNullOrEmpty(data))
                    {
                        languageData.Add(data);
                    }
                }
                return GetCompletenessLanguageData(languageData, languages);
            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, $"Error occurred in CalculateCompletenessForLanguage for Field Type Id {field}", ex, string.Empty, nameof(CalculateCompletenessForLanguage));
            }
            return 0;
        }

        private List<CultureInfo> ValidateLanguages(string languageCodes, List<CultureInfo> languageCultureInfo)
        {
            List<CultureInfo> languageLists = ExtensionService.Instance.GetAllLanguages();
            List<string> splitLanguageCodes = languageCodes.Split(',').ToList();

            foreach (string languageCode in splitLanguageCodes)
            {
                CultureInfo cultureLanguageCode = languageLists.Where(x => x.Name == languageCode.Trim()).FirstOrDefault();
                if (cultureLanguageCode != null)
                {
                    languageCultureInfo.Add(cultureLanguageCode);
                }
            }
           return languageCultureInfo;
        }
        #endregion

        #region Public Method
        public int GetCompletenessLanguageData(List<string> languageData, List<CultureInfo> languages)
        {
            int numberFieldIncomplete = 0;
            Decimal percentComplete = 0;
            
            numberFieldIncomplete = (languages.Count) - (languageData.Count);
            percentComplete = ((((Decimal)(languages.Count) - (Decimal)numberFieldIncomplete) / (Decimal)languages.Count) * new Decimal(100));

            return (int)percentComplete;
        }
        #endregion
    }
}
