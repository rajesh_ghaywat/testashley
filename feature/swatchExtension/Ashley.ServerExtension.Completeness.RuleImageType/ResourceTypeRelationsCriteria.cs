﻿using inRiver.Remoting.Dto;
using inRiver.Remoting.Log;
using inRiver.Remoting.Objects;
using inRiver.Server.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ashley.ServerExtension.Completeness.RuleResourceTypeRelations
{
    /// <summary>
    /// This is implementation of US 11.3 -Completeness Rule based on Image
    /// Items of type "Component" will not get any enrichment with images and marketing text.But they still need to be set to   completeness 100%, even if these fields are not enriched.
    /// Item of type "Item" or "Kit" will get enrich based on images and marketing text.
    ///     Ecom Image Ready Rule: yes to Scene7 Knockout, yes to Rollover Image, Yes AHS Image Set
    ///     Print Image Ready Rule:  yes Scene7 Knockout, yes LifeStyles
    ///     Google Manufacturer Center Image Ready Rule: yes Scene7 Knockout, yes to AFI Image Set
    /// </summary>
    public class ResourceTypeRelationsCriteria : ICompletenessCriteria
    {

        #region Private Constants
        private const string ENRICH_FIELDTYPEID = "EnrichFieldTypeId";
        private const string ENRICH_FIELDVALUES = "EnrichFieldValues";
        private const string RESOURCE_LINKTYPE = "ResourceLinkType";
        private const string IMAGETYPE_FIELDTYPEID = "ImageTypeFieldTypeId";
        private const string REQUIRED_IMAGETYPES = "RequiredImageTypes";
        private const char DELIMETER = ',';

        #endregion

        #region Private Variable

        private List<string> LstRequiredTypesOfTargetEntity = new List<string>();

        #endregion

        #region Interface Properties Implementation

        string ICompletenessCriteria.Name => "ResourceType Relations Complete";

        public List<string> SettingsKeys => new List<string> { ENRICH_FIELDTYPEID, ENRICH_FIELDVALUES, RESOURCE_LINKTYPE, IMAGETYPE_FIELDTYPEID, REQUIRED_IMAGETYPES };

        #endregion

        #region Interface Method Implementation

        /// <summary>
        /// Calculate the percentage based on Image type
        /// Also check the entity type: Items of type "Component" will not get any enrichment with images and marketing text.But they still need to be set to   completeness 100%, even if these fields are not enriched.
        /// </summary>
        /// <param name="entityId">Id of entity</param>
        /// <param name="settings">The setting key value collection</param>
        /// <returns>The calculated percentage from 0 to 100.</returns>
        int ICompletenessCriteria.GetCriteriaCompletenessPercentage(int entityId, List<CompletenessRuleSetting> settings)
        {

            try
            {
                if (!ValidatePreSettings(settings))
                {
                    return 0;
                }

                string entityTypeData = GetEntityType(settings, entityId);

                if (string.IsNullOrEmpty(entityTypeData))
                {
                    return 0;
                }

                CompletenessRuleSetting completenessRuleSetting = settings.FirstOrDefault(s => s.Key == ENRICH_FIELDVALUES);
                List<string> enrichFieldValues = completenessRuleSetting.Value.Split(DELIMETER).ToList();
                if (!enrichFieldValues.Contains(Convert.ToString(entityTypeData)))
                {
                    return 100;
                }

                return GetPercentageComplete(settings,entityId);

            }
            catch (Exception ex)
            {
                ExtensionService.Instance.Log(LogLevel.Error, nameof(ResourceTypeRelationsCriteria), ex, "", nameof(ICompletenessCriteria.GetCriteriaCompletenessPercentage));
            }

            return 0;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get Percentage Complete on the basis of image types associated with entity
        /// </summary>
        /// <param name="settings">The setting key value collection</param>
        /// <param name="entityId">Id of entity</param>
        /// <returns>The calculated percentage from 0 to 100.</returns>
        private int GetPercentageComplete(List<CompletenessRuleSetting> settings,int entityId)
        {
            int numberOfResourceTypeComplete = 0;
            float percentComplete = 0;

            CompletenessRuleSetting completenessRuleSettingRequiredTypesOfTargetEntity = settings.FirstOrDefault(s => s.Key == REQUIRED_IMAGETYPES);
            LstRequiredTypesOfTargetEntity = completenessRuleSettingRequiredTypesOfTargetEntity.Value.Split(DELIMETER).ToList();

            LstRequiredTypesOfTargetEntity.RemoveAll(type => String.IsNullOrEmpty(type));

            CompletenessRuleSetting completenessRuleSettingTargetLinks = settings.FirstOrDefault(s => s.Key == RESOURCE_LINKTYPE);

            CompletenessRuleSetting completenessRuleSettingImageFieldTypeId = settings.FirstOrDefault(s => s.Key == IMAGETYPE_FIELDTYPEID);

            List<DtoLink> targetLinks = ExtensionService.Instance.GetOutboundDtoLinksForEntityAndLinkType(entityId, completenessRuleSettingTargetLinks.Value).ToList();

            foreach (DtoLink targetLink in targetLinks)
            {
                Field targetEntityField = ExtensionService.Instance.GetField(targetLink.Target.Id, completenessRuleSettingImageFieldTypeId.Value);

                foreach (string requiredTypesOfTargetEntity in LstRequiredTypesOfTargetEntity)
                {
                    if (targetEntityField != null && targetEntityField.Data != null && requiredTypesOfTargetEntity.Equals(Convert.ToString(targetEntityField.Data), StringComparison.OrdinalIgnoreCase))
                    {
                        numberOfResourceTypeComplete++;
                        break;
                    }
                }
            }

            percentComplete = (((float)numberOfResourceTypeComplete / LstRequiredTypesOfTargetEntity.Count) * 100);

            return (int)percentComplete;
        }

        /// <summary>
        /// Validate all the configuration settings before calculating percentage complete
        /// </summary>
        /// <param name="settings">The setting key value collection</param>
        /// <returns>if all the settings are validated </returns>
        private bool ValidatePreSettings(List<CompletenessRuleSetting> settings)
        {
            return ValidateSettingValueExists(settings, RESOURCE_LINKTYPE) && ValidateSettingValueExists(settings, ENRICH_FIELDTYPEID) && ValidateSettingValueExists(settings, ENRICH_FIELDVALUES) && ValidateSettingValueExists(settings, REQUIRED_IMAGETYPES) && ValidateSettingValueExists(settings, REQUIRED_IMAGETYPES);
        }

        /// <summary>
        /// To check if the value of the setting is provided in completeness rule setup
        /// </summary>
        /// <param name="settings">The setting key value collection</param>
        /// <param name="settingName">The name of setting to validate</param>
        /// <returns>if setting is validated</returns>
        private bool ValidateSettingValueExists(List<CompletenessRuleSetting> settings, string settingName)
        {
            CompletenessRuleSetting completenessRuleSetting = settings.FirstOrDefault(s => s.Key == settingName);
            return !(completenessRuleSetting == null || string.IsNullOrEmpty(completenessRuleSetting.Value));

        }

        /// <summary>
        /// Get data for entitytype
        /// </summary>
        /// <param name="settings">The setting key value collection</param>
        /// <param name="entityId">Id of entity </param>
        /// <returns></returns>
        private string GetEntityType(List<CompletenessRuleSetting> settings, int entityId)
        {
            CompletenessRuleSetting completenessRuleSetting = settings.FirstOrDefault(s => s.Key == ENRICH_FIELDTYPEID);

            Field enrichField = ExtensionService.Instance.GetField(entityId, completenessRuleSetting.Value);

            return Convert.ToString(enrichField == null ? null : enrichField.Data);

        }

        #endregion
    }
}

